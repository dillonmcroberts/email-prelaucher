class UserMailer < ActionMailer::Base
  default from: "B&B <welcome@bedloe&blackwell.com>"
  def signup_email(user)
    @user = user
    @twitter_message = "#Beard care is evolving. Excited for @bedloe&blackwell to launch."

    mail(:to => user.email, :subject => "Thanks for signing up!")
  end
end
